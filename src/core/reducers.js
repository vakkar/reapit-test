import { combineReducers } from 'redux';
import gallery from './gallery/galleryReducer';

/**
 * ## CombineReducers
 *
 * the rootReducer will call each and every reducer with the state and action
 * EVERY TIME there is a basic action
 */
const appReducers = combineReducers({
    gallery
});

const rootReducer = (state, action) => {
    return appReducers(state, action);
};

export default rootReducer;
