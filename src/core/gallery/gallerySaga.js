import { call, put, takeEvery } from 'redux-saga/effects';
import { gallerySuccess, galleryFailure } from './galleryActions';

const { GALLERY_REQUEST } = require('./galleryActions').constants;

function* fetchGallery() {
    try {
        const response = require('../../movies.json');
        yield put(gallerySuccess(response));
    } catch (e) {
        yield put(galleryFailure(e.response ? e.response.data.message : e));
    }
}

/**
 * Watch actions
 */
export default function* gallerySaga() {
    yield* [
        takeEvery(GALLERY_REQUEST, fetchGallery)
    ];
}
