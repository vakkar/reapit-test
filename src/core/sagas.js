import { all } from 'redux-saga/effects';
import gallerySaga from './gallery/gallerySaga';

// single entry point to start all Sagas at once
export default function* rootSaga() {
    yield all([
        gallerySaga(),
    ]);
}
