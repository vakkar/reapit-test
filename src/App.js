import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { Row, Col } from 'antd';
import { map } from 'underscore';
import moment from 'moment';
import * as galleryActions from './core/gallery/galleryActions';
import "antd/dist/antd.css";
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { actions } = this.props;
        // load gallery
        actions.galleryRequest();
    }

    render() {
        const { gallery } = this.props;
        return (
          <div className="App">
              <Row gutter={12}>
                  {map(gallery, (row, index) => {
                    return (<Col key={index} xs={24} sm={12}>
                      <div className="gallery-item">
                          <div className="image-box">
                            <img width="100%" src={`assets/posters/${row.id}.jpg`} alt="" />
                          </div>
                          <div className="gallery-info-box">
                            <h2>{row.title}</h2>
                            <p>{moment(row.release_date, 'YYYY-MM-DD').format('DD/MM/YYYY')}</p>
                            <p>{row.overview}</p>
                          </div>
                        </div>
                    </Col>);
                  })}
              </Row>
          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
  return {
        ...ownProps,
        gallery: state.gallery.data
    };
}

const mapDispatchToProps = (dispatch) => {
  return {
       actions: bindActionCreators({
           ...galleryActions
       }, dispatch)
   };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
